# Generated by Django 2.0.3 on 2018-04-02 00:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inicio', '0006_auto_20180401_2024'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventos',
            name='descripcion',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='noticia',
            name='descripcion',
            field=models.TextField(),
        ),
    ]
