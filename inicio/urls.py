from django.urls import path 
from django.contrib.auth import views as auth_views 
from . import views 
from .views import (inicio, nosotros ,congregacion_femenina ,
                    congregacion_masculina , lista_eventos , detalle_evento , detalle_noticia ,
                    lista_noticias, lista_archivos , comisiones , fullcalendar, junta_directiva,
                    consejo_directivo, escuela_formacion, pastoral_vocacional,nuevas_generaciones,servicios, Asamblea,
                    lista_privada,)


urlpatterns = [
    path('', inicio.as_view() , name="inicio"),
    path('nosotros', nosotros.as_view(), name="nosotros"),
    path('congregacionf', congregacion_femenina.as_view(), name="congregacionf"),
    path('congregacionm', congregacion_masculina.as_view(), name="congregacionm"),
    path('eventos' , lista_eventos.as_view(), name="eventos"),
    path('detalle_evento/<int:pk>', detalle_evento.as_view(), name="detallev"),
    path('detalle_noticia/<int:pk>', detalle_noticia.as_view(), name="detallen"),
    path('lista_noticias', lista_noticias.as_view(), name="lista_noticias"),
    path('lista_archivos', lista_archivos.as_view(), name="lista_archivos"),
    path('comisiones', comisiones.as_view(), name="comisiones"),
    path('fullcalendar', fullcalendar.as_view(), name="fullcalendar"),
    path('junta_directiva', junta_directiva.as_view(), name="junta_directiva"),
    path('consejo_directivo', consejo_directivo.as_view(), name="consejo_directivo"),
    path('escuela_formacion', escuela_formacion.as_view(), name="escuela_formacion"),
    path('pastoral_vocacional', pastoral_vocacional.as_view(), name="pastoral_vocacional"),
    path('nuevas_generaciones', nuevas_generaciones.as_view(), name="nuevas_generaciones"),
    path('servicios', servicios.as_view(), name="servicios"),
    path('Asamblea', Asamblea.as_view(), name="Asamblea"),
    path('login', views.login_concepcionistas, name="login_concepcionistas"),
    path('lista_privada', lista_privada.as_view() , name="lista_privada"),


]