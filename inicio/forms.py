from .models import afiliados
from django import forms 


class afiliadoform(forms.ModelForm):
    class Meta:
        model = afiliados

        fields = ['formalizo_inscripcion_a', 'nombre','congregacion','correo','telefono','num_ref_deposito','fecha_deposito']
        labels = {'formalizo la inscripcion a ' : 'formalizo_inscripcion_a',
                    'Nombres y Apellidos' : 'nombre',
                    'congregacion': 'congregacion',
                    'correo' : 'correo',
                    'telefono' : 'telefono',
                    'numero de referencia del deposito': 'num_ref_deposito',
                    'fecha del deposito': 'fecha_deposito'}
        
        widgets = {'formalizo_insgripcion_a': forms.TextInput(attrs={'class':'form-control'}),
                    'nombre': forms.TextInput(attrs={'class':'form-control'}),
                    'congregacion': forms.TextInput(attrs={'class':'form-control'}),
                    'correo': forms.TextInput(attrs={'class':'form-control'}),
                    'telefono': forms.TextInput(attrs={'class':'form-control'}),
                    'num_ref_deposito': forms.TextInput(attrs={'class':'form-control'}),
                    'fecha_deposito': forms.DateInput(attrs={'class':'form-control'}),
        }       
