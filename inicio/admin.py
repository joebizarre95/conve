from django.contrib import admin
from .models import noticia ,congregaciones_masculinas , congregaciones_femeninas , eventos , afiliados , archivos , comisiones_servicios ,usuarios

# Register your models here.

@admin.register(noticia)
class AdminNoticia(admin.ModelAdmin):
    list_display = ('id', 'titulo','descripcion','foto','fecha_publicacion' )



@admin.register(congregaciones_femeninas)
class AdminCongregaciones_femeninas(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'correo', 'link','ubicacion','telefono')



@admin.register(congregaciones_masculinas)
class AdminCongregaciones_masculinas(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'correo', 'link','ubicacion','telefono')



@admin.register(eventos)
class AdminEventos(admin.ModelAdmin):
    list_display = ('id','titulo','descripcion','fecha_publicacion','foto')



@admin.register(afiliados)
class AdminAfiliados(admin.ModelAdmin):
    list_display = ('id', 'formalizo_inscripcion_a' , 'nombre', 'congregacion' , 'correo', 'telefono', 'num_ref_deposito' , 'fecha_deposito')


@admin.register(archivos)
class AdmiArchivos(admin.ModelAdmin):
    list_display = ('id','titulo','descripcion','foto','archivo',)



@admin.register(comisiones_servicios)
class AdminComisiones(admin.ModelAdmin):
    list_display = ('id','nombre',)


@admin.register(usuarios)
class AdmonUsuarios(admin.ModelAdmin):
	list_display = ('id','usuario','password','created_at',)


