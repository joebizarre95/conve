from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import TemplateView, CreateView, ListView, DetailView, DeleteView
from django.urls import reverse_lazy
from django.contrib.auth import authenticate, login , logout
from pprint import pprint
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import noticia , congregaciones_femeninas , congregaciones_masculinas , eventos , afiliados , archivos
from .forms import afiliadoform


class inicio(TemplateView):
    template_name = 'inicio/inicio.html'

    def get_context_data(self, *args, **kwargs):
        context = super(inicio, self).get_context_data(**kwargs)
        context['noticia'] = noticia.objects.order_by('-id')[:3]
        context['eventos'] = eventos.objects.order_by('-id')[:3]
        context['archivos'] = archivos.objects.order_by('-id')[:1]
        return context




class nosotros(TemplateView):
    template_name = 'inicio/nosotros.html'



class congregacion_masculina(ListView):
    model = congregaciones_masculinas
    template_name = 'inicio/lista_masculina.html'


class congregacion_femenina(ListView):
    model = congregaciones_femeninas
    template_name = 'inicio/lista_femenina.html'



class lista_eventos(CreateView):
    model = afiliados
    form_class = afiliadoform
    template_name = 'inicio/eventos.html'
    success_url = reverse_lazy('eventos')

    def get_context_data(self, *args, **kwargs):
        context = super(lista_eventos, self).get_context_data(**kwargs)
        context['eventos'] = eventos.objects.all()
        return context



class detalle_evento(DetailView):
    model = eventos
    template_name = 'inicio/evento_detalle.html'

    def get_context_data(self, *args, **kwargs):
        context = super(detalle_evento, self).get_context_data(**kwargs)
        context['mas_eventos'] = eventos.objects.order_by('-id')[:3]
        return context


class detalle_noticia(DetailView):
    model = noticia
    template_name = 'inicio/noticias_detalle.html'

    def get_context_data(self, *args, **kwargs):
        context = super(detalle_noticia, self).get_context_data(**kwargs)
        context['mas_noticias'] = noticia.objects.order_by('-id')[:3]
        return context


class lista_noticias(ListView):
    model = noticia 
    template_name = 'inicio/lista_noticias.html'



class lista_archivos(ListView):
    model = archivos
    template_name = 'inicio/lista_archivos.html'


class comisiones(TemplateView):
   template_name = 'inicio/comisiones.html'


class fullcalendar(TemplateView):
    template_name = 'inicio/fullcalendar.html'

    def get_context_data(self, *args, **kwargs):
        context = super(fullcalendar, self).get_context_data(**kwargs)
        context['eventos'] = eventos.objects.all()
        return context


class junta_directiva(TemplateView):
    template_name = 'inicio/junta_directiva.html'

class consejo_directivo(TemplateView):
    template_name = 'inicio/consejo_directivo.html'


class escuela_formacion(TemplateView):
    template_name = 'inicio/escuela_formacion.html'

    def get_context_data(self, *args, **kwargs):
        context = super(escuela_formacion, self).get_context_data(**kwargs)
        context['eventos'] = eventos.objects.filter(pertenece = 4)
        return context
    #escuela de formacion permanente debe de ser el id 4

class pastoral_vocacional(TemplateView):
    template_name = 'inicio/pastoral_vocacional.html'

    def get_context_data(self, *args, **kwargs):
        context = super(pastoral_vocacional, self).get_context_data(**kwargs)
        context['eventos'] = eventos.objects.filter(pertenece = 5)
        return context
    #pastoral vocacional debe ser el id 5


class nuevas_generaciones(TemplateView):
    template_name = 'inicio/nuevas_generaciones.html'

    def get_context_data(self, *args, **kwargs):
        context = super(nuevas_generaciones, self).get_context_data(**kwargs)
        context['eventos'] = eventos.objects.filter( partenece = 6)
        return context
    #nuevas generacioens debe ser el objeto 6

class servicios(TemplateView):
    template_name = 'inicio/servicios.html'

    def get_context_data(self, *args, **kwargs):
        context = super(servicios, self).get_context_data(**kwargs)
        context['eventos'] = eventos.objects.order_by('-id')
        return context


class Asamblea(TemplateView):
    template_name = 'inicio/asamblea.html'



def login_concepcionistas(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        return HttpResponseRedirect('/lista_privada')
    else:
        return HttpResponseRedirect('/')


    
class lista_privada(TemplateView):
    template_name = 'inicio/lista_privada.html'

    def get_context_data(self, *args, **kwargs):
        context = super(lista_privada, self).get_context_data(**kwargs)
        context['lista_masculina'] = congregaciones_masculinas.objects.all()
        context['lista_femenina'] = congregaciones_femeninas.objects.all()
        return context