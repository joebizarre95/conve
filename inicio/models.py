from django.db import models
from django.utils import timezone

# Create your models here.

class noticia(models.Model):
    titulo = models.CharField(max_length=255)
    descripcion = models.TextField()
    foto = models.ImageField(upload_to='',)
    fecha_publicacion = models.DateField(default=timezone.now)

    def __str__(self):
        return '{}'.format(
            self.titulo,
            self.descripcion,
            self.foto,
            self.fecha_publicacion,
        )
    
    class Meta:
        ordering = ('id',)


class congregaciones_femeninas(models.Model):
    nombre = models.CharField(max_length=255)
    correo = models.CharField(max_length=255, blank=True)
    link = models.CharField(max_length=255, blank=True)
    ubicacion = models.CharField(max_length=255, blank=True)
    telefono = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return '{}'.format(
            self.nombre
        )

    class Meta:
        ordering = ('id',)


class congregaciones_masculinas(models.Model):
    nombre = models.CharField(max_length=255)
    correo = models.CharField(max_length=255, blank=True)
    link = models.CharField(max_length=255, blank=True)
    ubicacion = models.CharField(max_length=255, blank=True)
    telefono = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return '{}'.format(
            self.nombre
        )

    class Meta:
        ordering = ('id',)


class eventos(models.Model):
    titulo = models.CharField(max_length=255)
    descripcion = models.TextField()
    foto = models.ImageField(upload_to='',)
    fecha_publicacion = models.DateField(default=timezone.now)
    pertenece = models.ForeignKey('comisiones_servicios' , blank=True, null=True, on_delete=False)

    def __str__(self):
        return '{}'.format(
            self.titulo,
            self.descripcion,
            self.fecha_publicacion
        )

    class Meta:
        ordering = ('id',)


class afiliados(models.Model):
    formalizo_inscripcion_a = models.CharField(max_length=255)
    nombre = models.CharField(max_length=255)
    congregacion = models.CharField(max_length=255)
    correo = models.CharField(max_length=255)
    telefono = models.IntegerField()
    num_ref_deposito = models.IntegerField()
    fecha_deposito = models.DateField(default=timezone.now)

    def __str__(self):
        return '{}'.format(
            self.formalizo_inscripcion_a,
            self.nombre,
            self.congregacion,
            self.correo,
            self.telefono,
            self.num_ref_deposito,
            self.fecha_deposito,
        )

    class Meta:
        ordering = ('id',)


class archivos(models.Model):
    titulo = models.CharField(max_length=255)
    descripcion = models.TextField()
    foto = models.ImageField(upload_to='', blank=True)
    archivo = models.FileField(upload_to='',)

    def __str__(self):
        return '{}'.format(
            self.titulo,
            self.descripcion,
            self.foto,
            self.archivo,
        )

    class Meta:
        ordering = ('id',)



class usuario_congregacion(models.Model):
    username = models.CharField(max_length=255)
    password = models.CharField(max_length=255)

    def __str__(self):
        return '{}'.format(
            self.username,
            self.password,)

    class Meta:
        ordering = ('id',)



class comisiones_servicios(models.Model):
    nombre = models.CharField(max_length=255)

    def __str__(self):
        return '{}'.format(
            self.nombre,)

    class Meta:
        ordering = ('id',)



class usuarios(models.Model):
    usuario = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    created_at = models.DateField(default=timezone.now)

    def __str__(self):
        return '{}'.format(
            self.usuario,
            self.password,)
        
    class Meta:
        ordering = ('id',)



